//
//  ContentView.swift
//  SwiftUIKiloLoki
//
//  Created by Arun CP on 30/09/21.
//

import SwiftUI

struct ContentView: View {
    //Image array
    var dataSource = ["image1","image2","image3","image4","image5",]
    //Add colour
    var facebookBlueColour = UIColor(red: 23/255.0, green: 120/255, blue: 242/255, alpha: 1)
    
    //Binding variable
    @Binding var search : String
    
    var body: some View {
        VStack{
            HStack{
                //Title and image
                Text("Facebook")
                    .font(.system(size: 40, weight: .bold, design: .default))
                    .foregroundColor(Color(facebookBlueColour))
                Spacer()
                Image(systemName: "person.circle")
                    .resizable()
                    .frame(width: 40, height: 40, alignment: .center)
                    .foregroundColor(Color(.secondaryLabel))
            }
            .padding()
            //Search field
            TextField("Search...", text: $search)
                .background(Color(.systemGray5))
                .cornerRadius(5)
                .padding(.horizontal, 12)
            //Story view
            ZStack{
                Color(.secondarySystemBackground)//This supports dark mode too.
                ScrollView(.vertical, showsIndicators: false, content: {
                    VStack{
                        //FB story
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack(spacing: 3){
                                ForEach(dataSource, id: \.self) { imageName in
                                    Image(imageName)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: 100, height: 130, alignment: .center)
                                        .clipped()
                                }
                            }
                            .padding()
                        }
                            //FB feeds
                            FBView(userName: "Arun", post: "I am going to win this race. There is no looking back", postImage: "image1", profileImage: "profile1")
                            FBView(userName: "Erric Jutner", post: "I am taking over this world with new technology inventions.", postImage: "image2", profileImage: "profile2")
                            FBView(userName: "Jeff Bezose", post: "Amazon can takeup this entire ecommerce ecosystem. This will be the begining of new world order.", postImage: "image3", profileImage: "profile3")
                    }
                })
            }
            Spacer()
        }
    }
}

struct FBView : View {
    
    var userName : String
    var post : String
    var postImage : String
    var profileImage : String
    
    var body : some View{
        VStack{
            //Profile image, name, time
            HStack{
                Image(profileImage)
                    .resizable()
                    .frame(width: 50, height: 50, alignment: .center)
                    .cornerRadius(25)
                VStack{
                    HStack{
                        Text(userName)
                            .font(.system(size: 22, weight: .semibold, design: .default))
                            .foregroundColor(Color.blue)
                        Spacer()
                    }
                    HStack{
                    Text("12 Minutes")
                        .font(.system(size: 14, weight: .regular, design: .default))
                    Spacer()
                }
            }
        }
        .padding()
            //
            //Actual post text and image
            VStack{
                HStack{
                    Text(post)
                        .font(.system(size: 22, weight: .regular, design: .default))
                        .multilineTextAlignment(.leading)
                    Spacer()
                }
                Image(postImage)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
            .padding(.horizontal, 20)
            //
            //Like, comment, share section
            HStack{//like comment share
                Button("Like") {
                    
                }
                Spacer()
                Button("Comment") {
                    
                }
                Spacer()
                Button("Share") {
                    
                }
            }
            .padding()
            //
        }
        .background(Color(.systemBackground))
        .cornerRadius(5)
        .padding(EdgeInsets(top: 5, leading: 10, bottom: 5, trailing: 10))
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(search: .constant(""))
    }
}
