//
//  SwiftUIKiloLokiApp.swift
//  SwiftUIKiloLoki
//
//  Created by Arun CP on 30/09/21.
//

import SwiftUI

@main
struct SwiftUIKiloLokiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(search: .constant(""))
        }
    }
}
